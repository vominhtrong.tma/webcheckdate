
public class Index {

	private int startRow;
	private int numberOfStudent;
	private char firstStartCell;
	private char secondStartCell;

	/**
	 * @param startRow
	 * @param numberOfStudent
	 * @param firstStartCell
	 * @param secondStartCell
	 */
	public Index(int startRow, int numberOfStudent, char firstStartCell, char secondStartCell) {
		super();
		this.startRow = startRow;
		this.numberOfStudent = numberOfStudent;
		this.firstStartCell = firstStartCell;
		this.secondStartCell = secondStartCell;
	}

	public Index ResultIndex(Object ob) {

		startRow = (int) ob;
		numberOfStudent = (int) ob;
		firstStartCell= (char) ChangeType((char) ob);		
		secondStartCell = (char) ChangeType((char) ob);
		
		return new Index(startRow,numberOfStudent,firstStartCell,secondStartCell);
	}

	private static int ChangeType(char kiTu) {
		char character = 'A';
		int viTri = 0;
		viTri += (int) Character.toUpperCase(kiTu) - (int) character;
		return viTri;

	}
}
